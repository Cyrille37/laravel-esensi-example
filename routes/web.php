<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\User ;

use Illuminate\Support\Facades\Validator;
use Proengsoft\JsValidation\Facades\JsValidatorFacade;

Route::get('/{user?}', function ( User $user = null )
{
    $jsValidator = JsValidatorFacade::validator(
        Validator::make( [], User::rules() ),
        '#form'
        );

    return view('welcome', ['user'=>$user,'jsValidator'=>$jsValidator]);

})->name('welcome');

Route::post('/{user?}', function ( Request $request, User $user = null )
{
    if( $user == null )
        $user = new User();
    $user->fill( $request->all() );
    $user->save();
    return redirect( route('welcome', ['user'=>$user]) );
});
