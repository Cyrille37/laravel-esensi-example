<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            /*.full-height {
                height: 100vh;
            }*/

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            form {
                margin: 5vh;
            }
            .alert-danger {
                color: red;
                font-weight: bolder;
            }
            .error-help-block {
                color: red;
                font-weight: bolder;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">

            <div class="content">
                <h1>
                    Laravel + esensi + proengsoft/laravel-jsvalidation
                </h1>

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/{{ $user->id??'' }}" method="POST" id="form">
    {{ csrf_field() }}
    <input name="id" type="hidden" value="{{ old('id',$user->id??'') }}"/>
    <input name="password" type="hidden" value="{{ old('user.password','secret') }}"/>
    <label for="name">Name:</label>
    <input name="name" type="text" id="name" value="{{ old('name',$user->name??'') }}"/>
    <br/>
    <label for="email">Email:</label>
    <input name="email" type="text" id="email" value="{{ old('email',$user->email??'') }}"/>
    <br/>
    <input type="submit" value="Save"/>
</form>

                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="/js/jsvalidation/jsvalidation.min.js"></script>

{!! $jsValidator !!}

    </body>
</html>
