
À l'occasion de [AFUP day 2020 Tours](https://event.afup.org/afup-day-2020/afup-day-2020-tours/)

Avec :
* laravel.com
* github.com/esensi/model
* jqueryvalidation.org/
* github.com/proengsoft/laravel-jsvalidation

et [Les slides](AFUP Day 2020 Tours - Valider sans copier-coller - Cyrille37.pdf)

```
$ composer install
$ ./artisan run
```
