<?php

namespace App;

use Esensi\Model\Model as EsensiModel;
use Illuminate\Database\Eloquent\Model as EloquentModel ;

class User extends EsensiModel
{
    /**
     * @see \Watson\Validating\ValidatingTrait
     * @var boolean
     */
    protected $throwValidationExceptions = true ;

    protected $rules = [
		'email' => ['required', 'email', 'unique'],
		'name' => [ 'required','string','min:5'],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

}
